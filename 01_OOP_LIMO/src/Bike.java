public class Bike {

    private String rider;
    private String color;
    private int direction;
    private double speed;
    private int MaxDegree = 45;
    private int MinDegree = -45;
   
    Bike() {
        setRider("NaN");
        setColor("NaN");
        setDirection(0);
        setSpeed(0);
    }
    
    Bike(String rider, String color) {
        setRider(rider);
        setColor(color);
        setDirection(0);
        setSpeed(0);
    }
    
    
    public void setRider(String rider) {
        this.rider = rider;                          
    }
    
    public void setColor(String color){
        this.color = color;
    };
    
    public void steer(int deltaR){
        setDirection(this.direction + deltaR);
    };

    public void setDirection(int direction) {
        if (direction >= this.MinDegree && direction <= this.MaxDegree) {
            this.direction = direction;
        } else if (direction < this.MinDegree) {
            this.direction = this.MinDegree;
        } else {
            this.direction = this.MaxDegree;
        }
    }    
        
    public void setSpeed(double speed) {
        if (speed >= 0) {
            this.speed = speed;
        } else {
            this.speed = 0;
        }
    }
        
    public void accelerate(double a, double sec){
        setSpeed(this.speed + a * sec);
    };
    
     public double getKmh() {
        return this.speed* 3.6;
    }
     
    @Override
    public String toString(){
        return "Rider: " + this.rider + " | " + "Color: " + this.color + " | " + "Direction: " + this.direction + " | " +  "kmh: " + getKmh();
    }; 

}