public class CityBike extends Bike {
     
    private boolean light;
    
    public CityBike(String color, String rider, boolean light) {
        super(rider, color);
        setLight(false);
    }
    public CityBike(boolean light ) {
       this.light = light;
    } 
  public void setLight(boolean light) {
        this.light = light;
    }
    public String getLight() {
        if(light == true) {
            return "on";
        }else {
            return "off";
        }
    }

    @Override
    public String toString(){
        return super.toString() + " | " + "Lights: " + getLight(); 
    }
}

