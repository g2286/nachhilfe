/*
@author LimoNathalia
*/

public class Main {
    public static void main(String args[]) {
        Bike b = new Bike("Strampler", "blue");
        b.steer(10);
        b.accelerate(0.3,9.8);
        System.out.println(b);

        Bike c = new CityBike("Thomas", "black", false);
        c.steer(10);
        c.accelerate(0.3,9.8);
        ((CityBike)c).setLight(true);
        System.out.println(c);
        }
}
